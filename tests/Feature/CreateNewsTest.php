<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

use App\Models\News;
use App\Models\User;

class CreateNewsTest extends TestCase
{

    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */

     public function setUp():void{
         parent::setUp();
         $this->user = User::factory()->create();
     }

    /** @test */
    public function authentificate_user_can_create_new_news()
    {
       $this->assertEquals(0,News::count());

       $data = [
           'title'=>'Саламалейкум',
           'body'=>'Здоровеньки было'
       ];

       $this->actingAs($this->user)->postJson(route('news.store'),$data)->assertStatus(201);

       $this->assertEquals(1,News::count());

       $news = News::first();

       $this->assertEquals($data['title'],$news->title);
       $this->assertEquals($data['body'],$news->body);
    }

    /** @test */
    public function unauthentificate_user_can_create_new_news()
    {
        $this->expectException(\Illuminate\Auth\AuthenticationException::class);
        $this->postJson(route('news.store'))->assertStatus(401);
    }

    /** @test */
    public function authentificate_user_can_create_new_news_with_images()
    {
        Storage::fake('public');

        $data = [
            'title'=>'Саламалейкум',
            'body'=>'Здоровеньки было',
            'image'=>$file = UploadedFile::fake()->image('image.jpg',1,1)
        ];

        $this->actingAs($this->user)->postJson(route('news.store'),$data)->assertStatus(201);

        $this->assertEquals(1,News::count());

        $news = News::first();


        $imagePath = 'news/'.$file->hashname();

        $this->assertEquals($imagePath,$news->image_path);

        Storage::assertExists($imagePath);

    }


}
