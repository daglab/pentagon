@extends('layouts.app')

@section('content')
<div class="row">
    <div class="ol-lg-12">LARAVEL CRUD</div>
    <a href="/products/create"><button type="button" class="btn btn-warning">Добавить товар</button></a>
</div>
<div class="row">
<table class="table table-hover table-dark">
<thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Description</th>
      <th scope="col">Price</th>
      <th scope="col">created_at</th>
      <th scope="col">updated_at</th>
      <th scope="col">Редактирование</th>
      <th scope="col">Удалить</th>
    </tr>
  </thead>
  <tbody>
    @foreach($products as $product)
  <tr>
      <th scope="row">{{$product->id}}</th>
      <td><a href="/products/{{$product->id}}">{{$product->name}}</a></td>
      <td>{{$product->description}}</td>
      <td>{{$product->price}}</td>
      <td>{{$product->created_at}}</td>
      <td>{{$product->updated_at}}</td>
      <td><a href="{{route('products.edit',$product->id)}}">Редактировать</td>
      <td><form method="POST" action="{{route('products.destroy',$product->id)}}">@csrf   <input type="hidden" name="_method" value="DELETE"><button>Удалить</button></form></td>
    </tr>
    @endforeach
    
  </tbody>
</table>
{{$products->links()}}
</div>

<style>
    img, svg {

    width: 30px;
}
</style>
@endsection