@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="ol-lg-12">LARAVEL CRUD</div>
        <a href="/products/create"><button type="button" class="btn btn-warning">Добавить товар</button></a>
    </div>
    <div class="row">
        @if ($errors->any())
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
        <form method="POST" action="/products/{{ $product->id }}">
            @csrf
            <input type="hidden" name="_method" value="PUT">
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">NAME</label>
                <input name="name" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                    value="{{ $product->name }}">
            </div>

            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">DESCRIPTION</label>
                <input name="description" type="text" class="form-control" id="exampleInputPassword1"
                    value="{{ $product->description }}">
            </div>

            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">PRICE</label>
                <input name="price" type="text" class="form-control" id="exampleInputPassword1"
                    value="{{ $product->price }}">
            </div>

            <button type="submit" class="btn btn-primary">SEND</button>
        </form>


    </div>
@endsection
