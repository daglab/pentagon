@extends('layouts.app')

@section('content')
<div class="row">
    <div class="ol-lg-12">LARAVEL CRUD</div>
    <a href="/products/create"><button type="button" class="btn btn-warning">Добавить товар</button></a>
</div>
<div class="row">

<form method="POST" action="/products">
@csrf
<div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">NAME</label>
    <input name="name" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
  </div>

  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">DESCRIPTION</label>
    <input name="description" type="text" class="form-control" id="exampleInputPassword1">
  </div>

  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">PRICE</label>
    <input name="price" type="text" class="form-control" id="exampleInputPassword1">
  </div>
  
  <button type="submit" class="btn btn-primary">SEND</button>
</form>


</div>


@endsection