<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class GoalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('goals')->insert([
            'user_id'=> 1,
            'number_of_goals'=> rand(1,99),
        ]);
        DB::table('goals')->insert([
            'user_id'=> 1,
            'number_of_goals'=> rand(1,99),
        ]);
        DB::table('goals')->insert([
            'user_id'=> 1,
            'number_of_goals'=> rand(1,99),
        ]);
        DB::table('goals')->insert([
            'user_id'=> 1,
            'number_of_goals'=> rand(1,99),
        ]);
        DB::table('goals')->insert([
            'user_id'=> 2,
            'number_of_goals'=> rand(1,99),
        ]);
        DB::table('goals')->insert([
            'user_id'=> 6,
            'number_of_goals'=> rand(1,99),
        ]);
    }
}
