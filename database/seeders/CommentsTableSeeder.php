<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->insert([
            'article_id'=> 1,
            'body'=> Str::random(150)
        ]);
        DB::table('comments')->insert([
            'article_id'=> 1,
            'body'=> Str::random(150)
        ]);
        DB::table('comments')->insert([
            'article_id'=> 1,
            'body'=> Str::random(150)
        ]);
        DB::table('comments')->insert([
            'article_id'=> 6,
            'body'=> Str::random(150)
        ]);
        
    }
}
