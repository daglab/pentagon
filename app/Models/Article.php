<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Str;

class Article extends Model
{
    use HasFactory;

    protected $fillable = ['title','description','body'];

  

    public function getRouteKeyName():string
   {
       return 'slug';
   }
   

    /**
     * Get the user that owns the Article
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function getFiltered(array $filters):Collection{
        return $this->where('title',$filters['title'])->get();
    }
    public function getTitleAttribute(string $title)
    {
        return Str::ucfirst($title);
    }

    public function setTitleAttribute(string $title):void
    {
        $this->attributes['title'] = $title;
        $this->attributes['slug'] = Str::slug($title);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }
/**
 * article_name
 * getArticleNameAttribute
 * 
 */
    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function questions(){
        return $this->morphMany(Question::class,'questionable');
    }
}
