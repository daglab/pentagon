<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;

    protected $fillable = ['body','user_id'];

    public function user(): belongsTo{
        return $this->belongsTo(User::class);
    }

    public function questionable(){
        return $this->morphTo();
    }
}
