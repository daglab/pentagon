<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['name','description','price'];
  //  protected $guarded = ['created_at','updated_at','id'];

    protected $attributes = ['price'=>100];
   
    protected $casts = ['price'=>'integer'];

    public function getRouteKeyName():string
    {
        return 'id';
    }
    
}
