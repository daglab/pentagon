<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Message\Whatsapp;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $serviceWhatsapp = $this->app->makeWith(Whatsapp::class,[
            'apiToken' => 'token'
        ]);

        $this->app->instance(MessageSender::class,$serviceWhatsapp);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
