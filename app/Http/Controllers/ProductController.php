<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       // $product = Product::where([['name','Новое 2'],['price','>',100]])->oldest()->limit(2)->get();
       $product = Product::distinct()->get(['name']);
        dd($product);

        $product = Product::firstWhere(function($query){
            $query->where('name','Новое 2')
                  ->where('price','>',100);
        });

    dd($product);


    



        $products = Product::orderBy('id')->paginate(4);
       // dd($products);
       return view('products.index',['products'=>$products]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

        /*
        $product = new Product();
        $product->name = 'Товар1';
        $product->description = 'Товар1 description';
        $product->price = 555;
        $product->save();
        */
        
       /*
        $product = new Product(['name'=>'Товар1','description'=>'Товар1 description','price'=>555]);
        $product->save();
       */
      /*
      $product = new Product();
      $products2 = $product->firstOrNew(['name'=>'Товар1','description'=>'Товар1 description','price'=>555]);
      $product->save();
      */

     /* $product = Product::withTrashed()->find([5,6,7,8]);*/

    //  dd($product);

     // $product->fill(['name'=>'Новое 2','description'=>'Товар2 description','price'=>755]);
    //$product->name = "Новое значение";
  //  $product->save();


   // $product->updateOrCreate(['name'=>'Новое 2'],['name'=>'Новое 2','description'=>'Товар2 description','price'=>777]);

//$product->delete();
   // $product->increment('price');

   // dd($product->wasChanged());

    //$product->save();

    //$product->restore();



  //  $product->forceDelete();

        /*
        $product2 = $product->replicate();
        $product2->fill(['name'=>'Новое 2','description'=>'Товар2 description','price'=>755]);
        $product2->save();
        */

   /* Product::insert(['name'=>'Новое 2','description'=>'Товар2 description','price'=>755],
    ['name'=>'Новое 2','description'=>'Товар2 description','price'=>755],
    ['name'=>'Новое 2','description'=>'Товар2 description','price'=>755],
    ['name'=>'Новое 2','description'=>'Товар2 description','price'=>755]);


    Product::where('price',755)->delete();

    Product::trancate();


*/
/*

$product = DB::connection('mysql')->table('products')->insert(['name'=>'Товар6','description'=>'Товар2 description','price'=>755]);

$product = Product::all(); 
  
*/
/* $product = Product::firstOrFail();

    $product = Product::firstOr(function(){
        return new Product(['name'=>'Новое 2','description'=>'Товар2 description','price'=>755]);
    });
*/
/*
            $product = Product::withCasts(['price'=>'int'])->when($request->name,
            function($query,$on){return $query->oldest();},
            function($query,$on){return $query->latest();}
            );

$products3 = $product->union($product1)->get();

        $request->validate([
            'name'=>'required|string',
            'description'=>'required|string',
           // 'price'=>'int'
        ]);
        Product::create($request->all());
        return redirect()->route('products.index');
    }
*/

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products.show',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('products.edit',compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $request->validate([
            'name'=>'required|string',
            'description'=>'required|string',
            'price'=>'numeric'
        ]);

        $product->update($request->all());
        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('products.index');
    }
}
