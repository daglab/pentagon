<?php

namespace App\Http\Controllers;

use App\Models\News;

use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function store(Request $request){

        $data= $request->validate([
            'title'=>'required|string|max:255',
            'body'=>'required|string',
            'image'=>'mimes:jpeg,png|nullable'
        ]);

        if (isset($data['image'])){
            $data['image_path'] = $data['image']->store('news');
        }


        $news = News::create($data);

        return response()->json($news,201);
    }
}
