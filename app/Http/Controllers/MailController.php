<?php

namespace App\Http\Controllers;
use App\Mail\TestMail;
use Illuminate\Support\Facades\Mail;

use App\Jobs\LongJob;

use Illuminate\Http\Request;

class MailController extends Controller
{
    public function send(Request $request){
       if($request->method()=='POST'){
            LongJob::dispatch()->onQueue('email');
       }
        return view('email.index');
    }
}
