<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Article\{IndexRequest,
                                StoreRequest,
                                UpdateRequest,
                                DestroyRequest};

use App\Http\Resources\ArticleCollection;
use App\Http\Resources\ArticleResource;

use App\Models\Article;
use App\Models\User;


class ArticleController extends Controller
{
    protected Article $article;
    protected User $user;

    public function __construct(Article $article,User $user){
        $this->article = $article;
        $this->$user = $user;
    }
   
    public function index(IndexRequest $request):ArticleCollection {

       // dd($request->validated());

        return new ArticleCollection($this->article->get()); //->getFiltered($request->validated())
    }


    public function show(Article $article) : ArticleResource
    {
        return new ArticleResource($article);
    }

    public function store(StoreRequest $request) : ArticleResource
    {
      
        $article = Article::create($request->validated()['article']);
        
        return new ArticleResource($article);
    }

    public function update(Article $article,UpdateRequest $request) : ArticleResource
    {
     //dd($request->validated());
        $article->update($request->validated()['article']);
        
        return new ArticleResource($article);
    }

    public function delete(Article $article,DestroyRequest $request) : void
    {
        $article->delete();
    }

}
