<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{

    public static $wrap = 'article';

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request):array
    {
        return [
            'slug'=>$this->slug,
            'title'=>$this->title,
            'category'=>$this->category->name,
            'description'=>$this->description,
            'body'=>$this->body,
            'created_at'=>$this->created_at,
            'updated_at'=>$this->updated_at
        ];
    }
}
