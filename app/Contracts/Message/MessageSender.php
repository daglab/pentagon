<?php

namespace App\Contracts\Message;

interface MessageSender{
    public function sendMessage();
    public function sendMessageWithUrl();
}



