<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ArticleController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::prefix('articles')->group(function(){
    Route::get('/',[ArticleController::class,'index']);
    Route::get('{article}',[ArticleController::class,'show']);
    Route::post('/',[ArticleController::class,'store']);
    Route::post('update/{article}',[ArticleController::class,'update']);
    Route::post('delete/{article}',[ArticleController::class,'delete']);
});

Route::prefix('salary')->group(function(){
Route::get('/calculate', [SalaryController::class,'index']);
Route::post('/calculate', [SalaryController::class,'create']);
});