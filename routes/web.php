<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\SendController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\WelcomeController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [WelcomeController::class,'index']);

Route::get('/a2/{id}', function ($id,Request $request) {
    return $id;
    return $request->method();
    return view('welcome');
});

Route::get('/myroute', function () {
    return view('my');
});


//Route::prefix('products')->group(function(){
    Route::resource('products',ProductController::class);
//});

Route::middleware('auth')->group(function(){
    Route::post('/news', [NewsController::class,'store'])->name('news.store');
});


Route::get('/send/test', [SendController::class,'sendTest']);

Route::match(['get','post'],'/send/test2', [MailController::class,'send']);


